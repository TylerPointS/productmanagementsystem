using Catalog.Data.Core.Models;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace PriceUpdateService.UnitTests
{
    public class PriceUpdateServiceTest : TestBase
    {
        [Fact]
        public void GetRootGroup_WhenGivenTree_ShouldReturnGroupWithParentZero()
        {
            // Arrange
            var service = new PriceUpdateService(_context);

            // Act
            var root = service.GetRootGroup();

            // Assert
            root.ParentId.Should().Be(0);
            root.Name.Should().Be("F"); //F is set as root in TestInitializer.Seed
        }

        [Fact]
        public void SetFixedCentValue_WhenGivenValueAndFixedCent_ShouldReturnValueWithFixedCent()
        {
            // Arrange
            var value = 10.25m;
            var fixedCentValue = 0.99m;
            var expected = 10.99m;

            // Act
            var result = PriceUpdateService.SetFixedCentValue(value, fixedCentValue);

            // Assert
            result.Should().Be(expected);
        }

        [Fact]
        public void GetTiresForPriceFilter_WhenGivenValidVehicleClass_ShouldReturnListOfVehicleClassTires()
        {
            // Arrange
            var service = new PriceUpdateService(_context);

            // Act
            var priceFilter = _context.PriceFilters.Find(9);
            var result = service.GetTiresForPriceFilter(priceFilter);
            var expected = new List<Tire>()
            {
                new Tire { Id = 29, Name = "Product 29", Description = "Description 29", Features = "Features 29", Benefits = "Benefits = 29", Brand = "Brand 6", Series = "Series 29", Model = "Model 29",  IdentificationNumber = "100029", QuantityPerUnit = 1, Length = null, Width = null, Height = null, Weight = null, DateCreated = new DateTime(2000, 01, 01, 12, 30, 30), DateCreatedByUserId = 1, DateLastModified = new DateTime(2000, 01, 01, 12, 30, 30), DateLastModifiedByUserId = 1, VehicleClass = "LT", SectionWidth = "215", SidewallAspectRatio = "55", WheelDiameter = "17", InternalConstruction = "R", LoadIndex = 98, SpeedRating = "V", PlyRating = "SL", Sidewall = "BSW", TreadDepth = "10", UniformTireQualityGrading = "500 AA A", Mileage = 80000, PartNumber = "MN234529", ManufacturerPartNumber = "234529", FederalExciseTax =     0 },
                new Tire { Id = 30, Name = "Product 30", Description = "Description 30", Features = "Features 30", Benefits = "Benefits = 30", Brand = "Brand 6", Series = "Series 30", Model = "Model 30",  IdentificationNumber = "100030", QuantityPerUnit = 1, Length = null, Width = null, Height = null, Weight = null, DateCreated = new DateTime(2000, 01, 01, 12, 30, 30), DateCreatedByUserId = 1, DateLastModified = new DateTime(2000, 01, 01, 12, 30, 30), DateLastModifiedByUserId = 1, VehicleClass = "LT", SectionWidth = "225", SidewallAspectRatio = "55", WheelDiameter = "17", InternalConstruction = "R", LoadIndex = 99, SpeedRating = "V", PlyRating = "SL", Sidewall = "BSW", TreadDepth = "10", UniformTireQualityGrading = "500 AA A", Mileage = 80000, PartNumber = "MN234530", ManufacturerPartNumber = "234530", FederalExciseTax =     0 }
            };

            //Assert
            result.Should().NotBeEmpty()
                .And.HaveCount(2)
                .And.ContainItemsAssignableTo<Tire>();

            result.Should().BeEquivalentTo(expected, options => options.ExcludingMissingMembers());
        }

        [Fact]
        public void GetTiresForPriceFilter_WhenGivenValidIdentificationNumber_ShouldReturnSpecifiedTire()
        {
            // Arrange
            var service = new PriceUpdateService(_context);

            // Act
            var priceFilter = _context.PriceFilters.Find(1);
            var result = service.GetTiresForPriceFilter(priceFilter);
            var expected = new List<Tire>()
            {
                new Tire { Id =  1, Name = "Product 01", Description = "Description 01", Features = "Features 01", Benefits = "Benefits = 01", Brand = "Brand 1", Series = "Series 01", Model = "Model 01",  IdentificationNumber = "100001", QuantityPerUnit = 1, Length = null, Width = null, Height = null, Weight = null, DateCreated = new DateTime(2000, 01, 01, 12, 30, 30), DateCreatedByUserId = 1, DateLastModified = new DateTime(2000, 01, 01, 12, 30, 30), DateLastModifiedByUserId = 1, VehicleClass =  "P", SectionWidth = "225", SidewallAspectRatio = "55", WheelDiameter = "17", InternalConstruction = "R", LoadIndex = 70, SpeedRating = "V", PlyRating = "SL", Sidewall = "BSW", TreadDepth = "10", UniformTireQualityGrading =  "740 A B", Mileage = 30000, PartNumber = "MN234501", ManufacturerPartNumber = "234501", FederalExciseTax =     0 },
            };

            //Assert
            result.Should().NotBeEmpty()
                .And.HaveCount(1)
                .And.ContainItemsAssignableTo<Tire>();

            result.Should().BeEquivalentTo(expected, options => options.ExcludingMissingMembers());
        }

        [Fact]
        public void GetTiresForPriceFilter_WhenGivenValidModel_ShouldReturnModelTires()
        {
            // Arrange
            var service = new PriceUpdateService(_context);

            // Act
            var priceFilter = _context.PriceFilters.Find(10);
            var result = service.GetTiresForPriceFilter(priceFilter);
            var expected = new List<Tire>()
            {
                new Tire { Id = 14, Name = "Product 14", Description = "Description 14", Features = "Features 14", Benefits = "Benefits = 14", Brand = "Brand 3", Series = "Series 14", Model = "Model 14",  IdentificationNumber = "100014", QuantityPerUnit = 1, Length = null, Width = null, Height = null, Weight = null, DateCreated = new DateTime(2000, 01, 01, 12, 30, 30), DateCreatedByUserId = 1, DateLastModified = new DateTime(2000, 01, 01, 12, 30, 30), DateLastModifiedByUserId = 1, VehicleClass =  "P", SectionWidth = "235", SidewallAspectRatio = "55", WheelDiameter = "17", InternalConstruction = "R", LoadIndex = 83, SpeedRating = "Q", PlyRating =  "E", Sidewall = "BSW", TreadDepth = "10", UniformTireQualityGrading =       null, Mileage = 40000, PartNumber = "MN234514", ManufacturerPartNumber = "234514", FederalExciseTax =  4.2m },
            };

            //Assert
            result.Should().NotBeEmpty()
                .And.HaveCount(1)
                .And.ContainItemsAssignableTo<Tire>();

            result.Should().BeEquivalentTo(expected, options => options.ExcludingMissingMembers());
        }

        [Fact]
        public void GetTiresForPriceFilter_WhenGivenValidBrand_ShouldReturnBrandTire()
        {
            // Arrange
            var service = new PriceUpdateService(_context);

            // Act
            var priceFilter = _context.PriceFilters.Find(8);
            var result = service.GetTiresForPriceFilter(priceFilter);
            var expected = new List<Tire>()
            {
                new Tire { Id = 11, Name = "Product 11", Description = "Description 11", Features = "Features 11", Benefits = "Benefits = 11", Brand = "Brand 3", Series = "Series 11", Model = "Model 11",  IdentificationNumber = "100011", QuantityPerUnit = 1, Length = null, Width = null, Height = null, Weight = null, DateCreated = new DateTime(2000, 01, 01, 12, 30, 30), DateCreatedByUserId = 1, DateLastModified = new DateTime(2000, 01, 01, 12, 30, 30), DateLastModifiedByUserId = 1, VehicleClass =  "P", SectionWidth = "245", SidewallAspectRatio = "55", WheelDiameter = "17", InternalConstruction = "R", LoadIndex = 80, SpeedRating = "Q", PlyRating =  "E", Sidewall = "BSW", TreadDepth = "11", UniformTireQualityGrading =  "740 A B", Mileage =     0, PartNumber = "MN234511", ManufacturerPartNumber = "234511", FederalExciseTax =  4.2m },
                new Tire { Id = 12, Name = "Product 12", Description = "Description 12", Features = "Features 12", Benefits = "Benefits = 12", Brand = "Brand 3", Series = "Series 12", Model = "Model 12",  IdentificationNumber = "100012", QuantityPerUnit = 1, Length = null, Width = null, Height = null, Weight = null, DateCreated = new DateTime(2000, 01, 01, 12, 30, 30), DateCreatedByUserId = 1, DateLastModified = new DateTime(2000, 01, 01, 12, 30, 30), DateLastModifiedByUserId = 1, VehicleClass =  "P", SectionWidth = "255", SidewallAspectRatio = "55", WheelDiameter = "17", InternalConstruction = "R", LoadIndex = 81, SpeedRating = "Q", PlyRating =  "E", Sidewall = "BSW", TreadDepth = "11", UniformTireQualityGrading =  "740 A B", Mileage = 40000, PartNumber = "MN234512", ManufacturerPartNumber = "234512", FederalExciseTax =  4.2m },
                new Tire { Id = 13, Name = "Product 13", Description = "Description 13", Features = "Features 13", Benefits = "Benefits = 13", Brand = "Brand 3", Series = "Series 13", Model = "Model 13",  IdentificationNumber = "100013", QuantityPerUnit = 1, Length = null, Width = null, Height = null, Weight = null, DateCreated = new DateTime(2000, 01, 01, 12, 30, 30), DateCreatedByUserId = 1, DateLastModified = new DateTime(2000, 01, 01, 12, 30, 30), DateLastModifiedByUserId = 1, VehicleClass =  "P", SectionWidth = "225", SidewallAspectRatio = "55", WheelDiameter = "17", InternalConstruction = "R", LoadIndex = 82, SpeedRating = "Q", PlyRating =  "E", Sidewall = "BSW", TreadDepth = "11", UniformTireQualityGrading =  "740 A B", Mileage = 40000, PartNumber = "MN234513", ManufacturerPartNumber = "234513", FederalExciseTax =  4.2m },
                new Tire { Id = 14, Name = "Product 14", Description = "Description 14", Features = "Features 14", Benefits = "Benefits = 14", Brand = "Brand 3", Series = "Series 14", Model = "Model 14",  IdentificationNumber = "100014", QuantityPerUnit = 1, Length = null, Width = null, Height = null, Weight = null, DateCreated = new DateTime(2000, 01, 01, 12, 30, 30), DateCreatedByUserId = 1, DateLastModified = new DateTime(2000, 01, 01, 12, 30, 30), DateLastModifiedByUserId = 1, VehicleClass =  "P", SectionWidth = "235", SidewallAspectRatio = "55", WheelDiameter = "17", InternalConstruction = "R", LoadIndex = 83, SpeedRating = "Q", PlyRating =  "E", Sidewall = "BSW", TreadDepth = "10", UniformTireQualityGrading =       null, Mileage = 40000, PartNumber = "MN234514", ManufacturerPartNumber = "234514", FederalExciseTax =  4.2m },
                new Tire { Id = 15, Name = "Product 15", Description = "Description 15", Features = "Features 15", Benefits = "Benefits = 15", Brand = "Brand 3", Series = "Series 15", Model = "Model 15",  IdentificationNumber = "100015", QuantityPerUnit = 1, Length = null, Width = null, Height = null, Weight = null, DateCreated = new DateTime(2000, 01, 01, 12, 30, 30), DateCreatedByUserId = 1, DateLastModified = new DateTime(2000, 01, 01, 12, 30, 30), DateLastModifiedByUserId = 1, VehicleClass =  "P", SectionWidth = "245", SidewallAspectRatio = "55", WheelDiameter = "17", InternalConstruction = "R", LoadIndex = 84, SpeedRating = "Q", PlyRating =  "E", Sidewall = "BSW", TreadDepth = "10", UniformTireQualityGrading =       null, Mileage = 40000, PartNumber = "MN234515", ManufacturerPartNumber = "234515", FederalExciseTax =  4.2m },
            };

            //Assert
            result.Should().NotBeEmpty()
                .And.HaveCount(5)
                .And.ContainItemsAssignableTo<Tire>();

            result.Should().BeEquivalentTo(expected, options => options.ExcludingMissingMembers());
        }

        [Fact]
        public void GetTiresForPriceFilter_WhenGivenValidUserProductTag_ShouldReturnTaggedTires()
        {
            // Arrange
            var service = new PriceUpdateService(_context);

            // Act
            var priceFilter = _context.PriceFilters.Find(11);
            var result = service.GetTiresForPriceFilter(priceFilter);
            var expected = new List<Tire>()
            {
                new Tire { Id =  7, Name = "Product 07", Description = "Description 07", Features = "Features 07", Benefits = "Benefits = 07", Brand = "Brand 2", Series = "Series 07", Model = "Model 07",  IdentificationNumber = "100007", QuantityPerUnit = 1, Length = null, Width = null, Height = null, Weight = null, DateCreated = new DateTime(2000, 01, 01, 12, 30, 30), DateCreatedByUserId = 1, DateLastModified = new DateTime(2000, 01, 01, 12, 30, 30), DateLastModifiedByUserId = 1, VehicleClass =  "P", SectionWidth = "245", SidewallAspectRatio = "55", WheelDiameter = "17", InternalConstruction = "R", LoadIndex = 76, SpeedRating = "h", PlyRating = "SL", Sidewall = "BSW", TreadDepth = "11", UniformTireQualityGrading =  "740 A B", Mileage = 40000, PartNumber = "MN234507", ManufacturerPartNumber = "234507", FederalExciseTax = 0.19m },
                new Tire { Id =  8, Name = "Product 08", Description = "Description 08", Features = "Features 08", Benefits = "Benefits = 08", Brand = "Brand 2", Series = "Series 08", Model = "Model 08",  IdentificationNumber = "100008", QuantityPerUnit = 1, Length = null, Width = null, Height = null, Weight = null, DateCreated = new DateTime(2000, 01, 01, 12, 30, 30), DateCreatedByUserId = 1, DateLastModified = new DateTime(2000, 01, 01, 12, 30, 30), DateLastModifiedByUserId = 1, VehicleClass =  "P", SectionWidth = "255", SidewallAspectRatio = "55", WheelDiameter = "17", InternalConstruction = "R", LoadIndex = 77, SpeedRating = "h", PlyRating =  "E", Sidewall = "BSW", TreadDepth = "11", UniformTireQualityGrading =  "740 A B", Mileage = 40000, PartNumber = "MN234508", ManufacturerPartNumber = "234508", FederalExciseTax =  4.2m },
                new Tire { Id =  9, Name = "Product 09", Description = "Description 09", Features = "Features 09", Benefits = "Benefits = 09", Brand = "Brand 2", Series = "Series 09", Model = "Model 09",  IdentificationNumber = "100009", QuantityPerUnit = 1, Length = null, Width = null, Height = null, Weight = null, DateCreated = new DateTime(2000, 01, 01, 12, 30, 30), DateCreatedByUserId = 1, DateLastModified = new DateTime(2000, 01, 01, 12, 30, 30), DateLastModifiedByUserId = 1, VehicleClass =  "P", SectionWidth = "225", SidewallAspectRatio = "55", WheelDiameter = "17", InternalConstruction = "R", LoadIndex = 78, SpeedRating = "Q", PlyRating =  "E", Sidewall = "BSW", TreadDepth = "11", UniformTireQualityGrading =  "740 A B", Mileage = 40000, PartNumber = "MN234509", ManufacturerPartNumber = "234509", FederalExciseTax =  4.2m },
                new Tire { Id = 10, Name = "Product 10", Description = "Description 10", Features = "Features 10", Benefits = "Benefits = 10", Brand = "Brand 2", Series = "Series 10", Model = "Model 10",  IdentificationNumber = "100010", QuantityPerUnit = 1, Length = null, Width = null, Height = null, Weight = null, DateCreated = new DateTime(2000, 01, 01, 12, 30, 30), DateCreatedByUserId = 1, DateLastModified = new DateTime(2000, 01, 01, 12, 30, 30), DateLastModifiedByUserId = 1, VehicleClass =  "P", SectionWidth = "235", SidewallAspectRatio = "55", WheelDiameter = "17", InternalConstruction = "R", LoadIndex = 79, SpeedRating = "Q", PlyRating =  "E", Sidewall = "BSW", TreadDepth = "11", UniformTireQualityGrading =  "740 A B", Mileage = 40000, PartNumber = "MN234510", ManufacturerPartNumber = "234510", FederalExciseTax =  4.2m },
                new Tire { Id = 11, Name = "Product 11", Description = "Description 11", Features = "Features 11", Benefits = "Benefits = 11", Brand = "Brand 3", Series = "Series 11", Model = "Model 11",  IdentificationNumber = "100011", QuantityPerUnit = 1, Length = null, Width = null, Height = null, Weight = null, DateCreated = new DateTime(2000, 01, 01, 12, 30, 30), DateCreatedByUserId = 1, DateLastModified = new DateTime(2000, 01, 01, 12, 30, 30), DateLastModifiedByUserId = 1, VehicleClass =  "P", SectionWidth = "245", SidewallAspectRatio = "55", WheelDiameter = "17", InternalConstruction = "R", LoadIndex = 80, SpeedRating = "Q", PlyRating =  "E", Sidewall = "BSW", TreadDepth = "11", UniformTireQualityGrading =  "740 A B", Mileage =     0, PartNumber = "MN234511", ManufacturerPartNumber = "234511", FederalExciseTax =  4.2m },
            };

            //Assert
            result.Should().NotBeEmpty()
                .And.HaveCount(5)
                .And.ContainItemsAssignableTo<Tire>();

            result.Should().BeEquivalentTo(expected, options => options.ExcludingMissingMembers());
        }

        [Fact]
        public void CalculateMargin_WhenGivenValidCostAndMarginRate_ShouldReturnRevenue()
        {

            //Arrange
            decimal cost = 75;
            decimal margin = .22m;

            //Act
            var result = PriceUpdateService.CalculateMargin(cost, margin);

            //Assert
            result.Should().Be(96.15m);
        }

        [Fact]
        public void CalculateMarkup_WhenGivenValidCostAndMarkupRate_ShouldReturnRevenue()
        {

            //Arrange
            decimal cost = 100;
            decimal markup = .22m;

            //Act
            var result = PriceUpdateService.CalculateMarkup(cost, markup);

            //Assert
            result.Should().Be(122);
        }

        [Fact] //Integration Test
        public void RunPriceUpdateService_WhenExecuted_ShouldCalculateAllPrices()
        {
            //Arrange
            var service = new PriceUpdateService(_context);

            //Act
            service.RunPriceUpdateService();
            var groupF = _context.GroupProductPrices.Where(p => p.GroupId == 6 && p.ProductId == 1 && p.Type == 0).SingleOrDefault();
            var groupB = _context.GroupProductPrices.Where(p => p.GroupId == 2 && p.ProductId == 1 && p.Type == 0).SingleOrDefault();
            var groupA = _context.GroupProductPrices.Where(p => p.GroupId == 1 && p.ProductId == 1 && p.Type == 0).SingleOrDefault();
            var groupG = _context.GroupProductPrices.Where(p => p.GroupId == 7 && p.ProductId == 1 && p.Type == 0).SingleOrDefault();
            var groupI = _context.GroupProductPrices.Where(p => p.GroupId == 8 && p.ProductId == 1 && p.Type == (PriceTypes)1).SingleOrDefault();

            //Assert
            groupF.Price.Should().Be(70.25m);
            groupB.Price.Should().Be(135.97m);
            groupA.Price.Should().Be(145.97m);
            groupI.Price.Should().Be(79.00m);
        }
    }
}
