﻿using Catalog.Data.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace PriceUpdateService.UnitTests
{
    public class TestBase : IDisposable
    {
        protected readonly CatalogDatabaseContext _context;

        public TestBase()
        {
            //Generate a new in memory database, can also use Guid
            var serviceProvider = new ServiceCollection()
            .AddEntityFrameworkInMemoryDatabase()
            .BuildServiceProvider();

            var options = new DbContextOptionsBuilder<CatalogDatabaseContext>()
                .UseInMemoryDatabase(databaseName: "InMemoryDatabase")
                .UseInternalServiceProvider(serviceProvider)
                .EnableSensitiveDataLogging()
                .Options;

            _context = new CatalogDatabaseContext(options);

            _context.Database.EnsureCreated();

            TestInitializer.Start(_context);
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();

            _context.Dispose();
        }
    }
}
