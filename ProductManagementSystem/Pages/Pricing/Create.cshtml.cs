﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Catalog.Data.Core.Models;
using Catalog.Data.Persistence;

namespace ProductManagementSystem.Pages.Pricing
{
    public class CreateModel : PageModel
    {
        private readonly Catalog.Data.Persistence.CatalogDatabaseContext _context;

        public CreateModel(Catalog.Data.Persistence.CatalogDatabaseContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            var groups = _context.Groups.ToList();
            GroupList = new List<SelectListItem>();

            foreach (var group in groups)
            {
                GroupList.Add(new SelectListItem
                {
                    Text = group.Name,
                    Value = group.Id.ToString()
                });
            }

            return Page();
        }

        [BindProperty]
        public PriceFilter PriceFilter { get; set; }
        public List<SelectListItem> GroupList { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.PriceFilters.Add(PriceFilter);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}