﻿using Catalog.Data.Core.Models;
using Catalog.Data.Persistence;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagementSystem.Pages.Tires
{
    public class ImportModel : PageModel
    {
        private IHostingEnvironment _hostingEnvironment;
        private readonly CatalogDatabaseContext _context;
        private readonly UnitOfWork _unitOfWork;

        public ImportModel(CatalogDatabaseContext context, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;
            _unitOfWork = new UnitOfWork(_context);
        }

        public void OnGet()
        {
        }

        public ActionResult OnPostImport()
        {
            IFormFile file = Request.Form.Files[0];

            string folderName = "Upload";

            string webRootPath = _hostingEnvironment.WebRootPath;

            string newPath = Path.Combine(webRootPath, folderName);

            StringBuilder sb = new StringBuilder();

            if (!Directory.Exists(newPath))

            {
                Directory.CreateDirectory(newPath);
            }

            if (file.Length > 0)

            {
                string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                ISheet sheet;
                string fullPath = Path.Combine(newPath, file.FileName);
                var tires = new List<Tire>();

                using (var stream = new FileStream(fullPath, FileMode.Create))

                {
                    file.CopyTo(stream);

                    stream.Position = 0;

                    if (sFileExtension == ".xls")

                    {
                        HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //This will read the Excel 97-2000 formats

                        sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook
                    }
                    else

                    {
                        XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //This will read 2007 Excel format

                        sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook
                    }

                    IRow headerRow = sheet.GetRow(0); //Get Header Row

                    int cellCount = headerRow.LastCellNum;

                    sb.Append("<table class='table'><tr>");

                    for (int j = 0; j < cellCount; j++)

                    {
                        NPOI.SS.UserModel.ICell cell = headerRow.GetCell(j);

                        if (cell == null || string.IsNullOrWhiteSpace(cell.ToString())) continue;

                        sb.Append("<th>" + cell.ToString() + "</th>");

                        //Validate proper headers
                        string[] validHeaders =
                        {
                            "partnumber",
                            "manufacturerpartnumber"
                        };

                        if (!Array.Exists(validHeaders, element => element == cell.ToString()))
                        {
                            //Error unknown header
                        }
                    }

                    sb.Append("</tr>");

                    sb.AppendLine("<tr>");

                    for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File

                    {
                        IRow row = sheet.GetRow(i);
                        
                        var tire = new Tire();

                        if (row == null) continue;

                        if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;

                        for (int j = row.FirstCellNum; j < cellCount; j++)

                        {
                            if (row.GetCell(j) != null)

                                sb.Append("<td>" + row.GetCell(j).ToString() + "</td>");

                            var header = headerRow.GetCell(j).StringCellValue;
                            switch (header)
                            {
                                case "ourpartnum":
                                    tire.PartNumber = row.GetCell(j).ToString();
                                    break;
                                case "manufacturerpartnumber":
                                    tire.ManufacturerPartNumber = row.GetCell(j).ToString();
                                    break;
                                case "description":
                                    tire.Description = row.GetCell(j).ToString();
                                    break;
                                case "features":
                                    tire.Features = row.GetCell(j).ToString();
                                    break;
                                case "benefits":
                                    tire.Benefits = row.GetCell(j).ToString();
                                    break;
                                case "brand":
                                    tire.Brand = row.GetCell(j).ToString();
                                    break;
                                case "series":
                                    tire.Series = row.GetCell(j).ToString();
                                    break;
                                case "model":
                                    tire.Model = row.GetCell(j).ToString();
                                    break;
                                case "length":
                                    tire.Length = (decimal)row.GetCell(j).NumericCellValue;
                                    break;
                                case "width":
                                    tire.Width = (decimal)row.GetCell(j).NumericCellValue;
                                    break;
                                case "height":
                                    tire.Height = (decimal)row.GetCell(j).NumericCellValue;
                                    break;
                                case "weight":
                                    tire.Weight = (decimal)row.GetCell(j).NumericCellValue;
                                    break;
                                case "tireprefix":
                                    tire.Prefix = row.GetCell(j).ToString();
                                    break;
                                case "sectionwidth":
                                    tire.SectionWidth = row.GetCell(j).ToString();
                                    break;
                                case "aspectratio":
                                    tire.SidewallAspectRatio = new string(row.GetCell(j).ToString().Where(c => Char.IsDigit(c)).ToArray());
                                    break;
                                case "wheeldiameter":
                                    tire.WheelDiameter = row.GetCell(j).ToString();
                                    break;
                                case "internalconstruction":
                                    tire.InternalConstruction = row.GetCell(j).ToString();
                                    break;
                                case "loadindex":
                                    tire.LoadIndex = (int)row.GetCell(j).NumericCellValue;
                                    break;
                                case "maxpsi":
                                    tire.MaximumTirePressure = (int)row.GetCell(j).NumericCellValue;
                                    break;
                                case "speedrating":
                                    tire.SpeedRating = row.GetCell(j).ToString();
                                    break;
                                case "plyrating":
                                    tire.PlyRating = row.GetCell(j).ToString();
                                    break;
                                case "sidewall":
                                    tire.Sidewall = row.GetCell(j).ToString();
                                    break;
                                case "utqg":
                                    tire.UniformTireQualityGrading = row.GetCell(j).ToString();
                                    break;
                                case "treaddepth":
                                    tire.TreadDepth = row.GetCell(j).ToString();
                                    break;
                                case "mileage":
                                    tire.Mileage = (int)row.GetCell(j).NumericCellValue;
                                    break;
                                case "fet":
                                    tire.FederalExciseTax = (decimal)row.GetCell(j).NumericCellValue;
                                    break;
                                case "tiretype":
                                    tire.VehicleClass = row.GetCell(j).ToString();
                                    break;
                                default:
                                    break;
                            }

                            //Validate data
                            //Add data
                            
                        }
                        tire.Name = tire.Brand + " " + tire.Series;
                        tire.DateCreated = DateTime.UtcNow;
                        tire.DateCreatedByUserId = 1;
                        tire.DateLastModified = DateTime.UtcNow;
                        tire.DateLastModifiedByUserId = 1;

                        tires.Add(tire);
                        
                        sb.AppendLine("</tr>");
                    }
                    _unitOfWork.Tires.AddRange(tires);
                    _unitOfWork.Complete();
                    sb.Append("</table>");
                }
            }

            return this.Content(sb.ToString());
        }

        public async Task<IActionResult> OnPostExport()
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = "Price_List_" + DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString() + DateTime.Today.Day.ToString() + ".xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();

            using (var fs = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook;

                workbook = new XSSFWorkbook();

                ISheet excelSheet = workbook.CreateSheet("Point S Price");

                IRow row = excelSheet.CreateRow(0);

                row.CreateCell(0).SetCellValue("Group Name");
                row.CreateCell(1).SetCellValue("Manufacturer Number");
                row.CreateCell(2).SetCellValue("Product Name");
                row.CreateCell(3).SetCellValue("Price");

                row = excelSheet.CreateRow(1);
                row.CreateCell(0).SetCellValue("Member Group");
                row.CreateCell(1).SetCellValue("VEN123");
                row.CreateCell(2).SetCellValue("Cooper CS5 Ultra Touring");
                row.CreateCell(3).SetCellValue("199.99");

                row = excelSheet.CreateRow(2);
                row.CreateCell(0).SetCellValue("Member Group");
                row.CreateCell(1).SetCellValue("VEN456");
                row.CreateCell(2).SetCellValue("Hankook Ventus S1 noble2");
                row.CreateCell(3).SetCellValue("229.99");

                row = excelSheet.CreateRow(3);
                row.CreateCell(0).SetCellValue("Member Group");
                row.CreateCell(1).SetCellValue("VEN789");
                row.CreateCell(2).SetCellValue("Nokian Zline");
                row.CreateCell(3).SetCellValue("349.99");

                workbook.Write(fs);
            }

            using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }

            memory.Position = 0;

            return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);
        }
    }
}