﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Catalog.Data.Core.Models;
using Catalog.Data.Persistence;

namespace ProductManagementSystem.Pages.Tires
{
    public class EditModel : PageModel
    {
        private readonly Catalog.Data.Persistence.CatalogDatabaseContext _context;

        public EditModel(Catalog.Data.Persistence.CatalogDatabaseContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Tire Tire { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tire = await _context.Tires.SingleOrDefaultAsync(m => m.Id == id);

            if (Tire == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Tire).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TireExists(Tire.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool TireExists(int id)
        {
            return _context.Tires.Any(e => e.Id == id);
        }
    }
}
