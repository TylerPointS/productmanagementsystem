﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Catalog.Data.Core.Models;
using Catalog.Data.Persistence;

namespace ProductManagementSystem.Pages.Tires
{
    public class DetailsModel : PageModel
    {
        private readonly Catalog.Data.Persistence.CatalogDatabaseContext _context;

        public DetailsModel(Catalog.Data.Persistence.CatalogDatabaseContext context)
        {
            _context = context;
        }

        public Tire Tire { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tire = await _context.Tires.SingleOrDefaultAsync(m => m.Id == id);

            if (Tire == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
