﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Catalog.Data.Core.Models;
using Catalog.Data.Persistence;

namespace ProductManagementSystem.Pages.Tires
{
    public class CreateModel : PageModel
    {
        private readonly Catalog.Data.Persistence.CatalogDatabaseContext _context;

        public CreateModel(Catalog.Data.Persistence.CatalogDatabaseContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Tire Tire { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var newTire = new Tire();

            if (await TryUpdateModelAsync<Tire>(
                newTire,
                "tire",
                t => t.Benefits,
                t => t.Brand,
                t => t.Description,
                t => t.Features,
                t => t.FederalExciseTax,
                t => t.Height,
                t => t.InternalConstruction,
                t => t.Length,
                t => t.LoadIndex,
                t => t.ManufacturerPartNumber,
                t => t.MaximumTirePressure,
                t => t.Mileage,
                t => t.Model,
                t => t.Name,
                t => t.PartNumber,
                t => t.PlyRating,
                t => t.Prefix,
                t => t.QuantityPerUnit,
                t => t.SectionWidth,
                t => t.Series,
                t => t.Sidewall,
                t => t.SidewallAspectRatio,
                t => t.Size,
                t => t.SpeedRating,
                t => t.TreadDepth,
                t => t.UniformTireQualityGrading,
                t => t.VehicleClass,
                t => t.Weight,
                t => t.WheelDiameter,
                t => t.Width
                ))
            {
                Tire.DateCreated = DateTime.UtcNow;
                Tire.DateCreatedByUserId = 1; //TODO Change hardcoding to actual user id
                Tire.DateLastModified = DateTime.UtcNow;
                Tire.DateLastModifiedByUserId = 1; //TODO Change hardcoding to actual user id

                _context.Tires.Add(Tire);
                await _context.SaveChangesAsync();

                return RedirectToPage("./Index");
            }

            return null;
        }
    }
}