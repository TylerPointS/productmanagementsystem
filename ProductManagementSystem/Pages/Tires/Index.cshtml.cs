﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Catalog.Data.Core.Models;
using Catalog.Data.Persistence;

namespace ProductManagementSystem.Pages.Tires
{
    public class IndexModel : PageModel
    {
        private readonly CatalogDatabaseContext _context;

        public IndexModel(CatalogDatabaseContext context)
        {
            _context = context;
        }

        public PaginatedList<Tire> Tire { get;set; }
        public string NameSort { get; set; }
        public string DateSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }

        public async Task OnGetAsync(string sortOrder, string currentFilter, string searchString, int? pageIndex)
        {
            CurrentSort = sortOrder;

            if(searchString != null)
            {
                pageIndex = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            CurrentFilter = searchString;
            IQueryable<Tire> tireQ = _context.Tires;

            if(!String.IsNullOrEmpty(searchString))
            {
                tireQ = tireQ
                    .Where(s => s.Name.Contains(searchString) || s.Brand.Contains(searchString) || s.Series.Contains(searchString) || s.Description.Contains(searchString) || s.Features.Contains(searchString))
                    .OrderByDescending(s => s.DateCreated);
            }

            int pageSize = 10;

            Tire = await PaginatedList<Tire>.CreateAsync(tireQ.AsNoTracking(), pageIndex ?? 1, pageSize);
        }
    }
}
