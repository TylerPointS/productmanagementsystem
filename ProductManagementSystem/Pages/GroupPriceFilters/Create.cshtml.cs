﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Catalog.Data.Core.Models;
using Catalog.Data.Persistence;

namespace ProductManagementSystem.Pages.GroupPriceFilters
{
    public class CreateModel : PageModel
    {
        private readonly Catalog.Data.Persistence.CatalogDatabaseContext _context;

        public CreateModel(Catalog.Data.Persistence.CatalogDatabaseContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["GroupId"] = new SelectList(_context.Groups, "Id", "Name");
        ViewData["PriceFilterId"] = new SelectList(_context.PriceFilters, "Id", "Id");
            return Page();
        }

        [BindProperty]
        public GroupPriceFilter GroupPriceFilter { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.GroupPriceFilters.Add(GroupPriceFilter);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}