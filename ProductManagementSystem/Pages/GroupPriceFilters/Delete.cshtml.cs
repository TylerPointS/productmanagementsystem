﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Catalog.Data.Core.Models;
using Catalog.Data.Persistence;

namespace ProductManagementSystem.Pages.GroupPriceFilters
{
    public class DeleteModel : PageModel
    {
        private readonly Catalog.Data.Persistence.CatalogDatabaseContext _context;

        public DeleteModel(Catalog.Data.Persistence.CatalogDatabaseContext context)
        {
            _context = context;
        }

        [BindProperty]
        public GroupPriceFilter GroupPriceFilter { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            GroupPriceFilter = await _context.GroupPriceFilters
                .Include(g => g.Group)
                .Include(g => g.PriceFilter).SingleOrDefaultAsync(m => m.GroupId == id);

            if (GroupPriceFilter == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            GroupPriceFilter = await _context.GroupPriceFilters.FindAsync(id);

            if (GroupPriceFilter != null)
            {
                _context.GroupPriceFilters.Remove(GroupPriceFilter);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
