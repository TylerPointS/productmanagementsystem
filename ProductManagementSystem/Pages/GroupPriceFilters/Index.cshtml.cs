﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Catalog.Data.Core.Models;
using Catalog.Data.Persistence;

namespace ProductManagementSystem.Pages.GroupPriceFilters
{
    public class IndexModel : PageModel
    {
        private readonly Catalog.Data.Persistence.CatalogDatabaseContext _context;

        public IndexModel(Catalog.Data.Persistence.CatalogDatabaseContext context)
        {
            _context = context;
        }

        public IList<GroupPriceFilter> GroupPriceFilter { get;set; }

        public async Task OnGetAsync()
        {
            GroupPriceFilter = await _context.GroupPriceFilters
                .Include(g => g.Group)
                .Include(g => g.PriceFilter).ToListAsync();
        }
    }
}
