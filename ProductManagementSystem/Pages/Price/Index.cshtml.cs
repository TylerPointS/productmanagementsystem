﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Catalog.Data.Core.Models;
using Catalog.Data.Persistence;

namespace ProductManagementSystem.Pages.Price
{
    public class IndexModel : PageModel
    {
        private readonly Catalog.Data.Persistence.CatalogDatabaseContext _context;

        public IndexModel(Catalog.Data.Persistence.CatalogDatabaseContext context)
        {
            _context = context;
        }

        public IList<GroupTirePrice> GroupTirePrice { get;set; }

        public async Task OnGetAsync()
        {
            GroupTirePrice = await _context.GroupTirePrices
                .Include(g => g.Group)
                .Include(g => g.Tire).ToListAsync();
        }
    }
}
