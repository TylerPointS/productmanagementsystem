using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catalog.Data.Core.Models;
using Catalog.Data.Persistence;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace ProductManagementSystem.Pages.Price
{
    public class ImportModel : PageModel
    {
        private IHostingEnvironment _hostingEnvironment;
        private readonly CatalogDatabaseContext _context;
        private readonly UnitOfWork _unitOfWork;

        public ImportModel(CatalogDatabaseContext context, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;
            _unitOfWork = new UnitOfWork(_context);
        }

        public void OnGet()
        {
        }

        public ActionResult OnPostImport()
        {
            IFormFile file = Request.Form.Files[0];
            string folderName = "Upload";
            string webRootPath = _hostingEnvironment.WebRootPath;
            string newPath = Path.Combine(webRootPath, folderName);
            StringBuilder sb = new StringBuilder();

            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }

            if (file.Length > 0)
            {
                string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                ISheet sheet;
                string fullPath = Path.Combine(newPath, file.FileName);
                var products = new List<GroupTirePrice>();

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                    stream.Position = 0;

                    if (sFileExtension == ".xls")
                    {
                        HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //This will read the Excel 97-2000 formats

                        sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook
                    }
                    else
                    {
                        XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //This will read 2007 Excel format

                        sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook
                    }

                    IRow headerRow = sheet.GetRow(0); //Get Header Row
                    int cellCount = headerRow.LastCellNum;

                    sb.Append("<table class='table'><tr>");

                    for (int j = 0; j < cellCount; j++)
                    {
                        NPOI.SS.UserModel.ICell cell = headerRow.GetCell(j);

                        if (cell == null || string.IsNullOrWhiteSpace(cell.ToString())) continue;

                        sb.Append("<th>" + cell.ToString() + "</th>");

                        //Validate proper headers
                        string[] validHeaders =
                        {
                            "partnumber",
                            "manufacturerpartnumber",
                            "price"
                        };

                        if (!Array.Exists(validHeaders, element => element == cell.ToString()))
                        {
                            //Error unknown header
                        }
                    }

                    sb.Append("</tr>");

                    sb.AppendLine("<tr>");

                    for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                    {
                        IRow row = sheet.GetRow(i);
                        var price = new GroupTirePrice();

                        if (row == null) continue;

                        if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;

                        for (int j = row.FirstCellNum; j < cellCount; j++)
                        {
                            if (row.GetCell(j) != null)
                            {
                                sb.Append("<td>" + row.GetCell(j).ToString() + "</td>");
                            }

                            var header = headerRow.GetCell(j).StringCellValue;

                            switch (header)
                            {
                                case "ourpartnum":
                                    break;

                                case "manufacturerpartnum":
                                    var tire = _unitOfWork.Tires.Find(x => x.ManufacturerPartNumber == row.GetCell(j).ToString()).SingleOrDefault();
                                    var group = _unitOfWork.Groups.Get(1);

                                    var priceFilter = new PriceFilter();

                                    priceFilter.IsActive = true;
                                    priceFilter.Name = tire.Name.ToString();
                                    priceFilter.UserId = 1;
                                    priceFilter.PriceType = PriceTypes.Default;
                                    //Find maximum order for existing prices for group tire price

                                    if (tire != null)
                                    {
                                        var selection = new SelectionCriteria();
                                        selection.Source = "Product";
                                        selection.Name = "IdentificationNumber";
                                        selection.Value = tire.IdentificationNumber.ToString();
                                        selection.PriceFilter = priceFilter;
                                    }
                                    break;

                                case "price":
                                    break;

                                default:
                                    break;
                            }
                            //Validate data
                            //Add data
                        }
                        products.Add(price);

                        sb.AppendLine("</tr>");
                    }
                    _unitOfWork.GroupTirePrices.AddRange(products);
                    _unitOfWork.Complete();
                    sb.Append("</table>");
                }
            }

            return this.Content(sb.ToString());
        }

        public async Task<IActionResult> OnPostExport()
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = "Price_List_" + DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString() + DateTime.Today.Day.ToString() + ".xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();

            using (var fs = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook;

                workbook = new XSSFWorkbook();

                ISheet excelSheet = workbook.CreateSheet("Point S Price");

                var prices = _unitOfWork.GroupProductPrices;

                var rowNumber = 0;
                IRow row = excelSheet.CreateRow(rowNumber);
                row.CreateCell(0).SetCellValue("Group");
                row.CreateCell(1).SetCellValue("Manufacturer Number");
                row.CreateCell(2).SetCellValue("Product Name");
                row.CreateCell(3).SetCellValue("Price");

                foreach (var price in prices.GetAllWIthProducts())
                {
                    rowNumber += 1;

                    row = excelSheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(price.Group.ToString());
                    row.CreateCell(1).SetCellValue(price.Product.ToString());
                    row.CreateCell(2).SetCellValue(_unitOfWork.Tires.Get(price.Product.Id).ManufacturerPartNumber.ToString());
                    row.CreateCell(3).SetCellValue(price.Price.ToString());
                }

                workbook.Write(fs);
            }

            using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }

            memory.Position = 0;

            return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);
        }
    }
}