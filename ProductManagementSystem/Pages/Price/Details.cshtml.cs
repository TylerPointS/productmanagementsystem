﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Catalog.Data.Core.Models;
using Catalog.Data.Persistence;

namespace ProductManagementSystem.Pages.Price
{
    public class DetailsModel : PageModel
    {
        private readonly Catalog.Data.Persistence.CatalogDatabaseContext _context;

        public DetailsModel(Catalog.Data.Persistence.CatalogDatabaseContext context)
        {
            _context = context;
        }

        public GroupTirePrice GroupTirePrice { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            GroupTirePrice = await _context.GroupTirePrices
                .Include(g => g.Group)
                .Include(g => g.Tire).SingleOrDefaultAsync(m => m.GroupId == id);

            if (GroupTirePrice == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
