﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Catalog.Data.Core.Models;
using Catalog.Data.Persistence;

namespace ProductManagementSystem.Pages.Price
{
    public class EditModel : PageModel
    {
        private readonly Catalog.Data.Persistence.CatalogDatabaseContext _context;

        public EditModel(Catalog.Data.Persistence.CatalogDatabaseContext context)
        {
            _context = context;
        }

        [BindProperty]
        public GroupTirePrice GroupTirePrice { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            GroupTirePrice = await _context.GroupTirePrices
                .Include(g => g.Group)
                .Include(g => g.Tire).SingleOrDefaultAsync(m => m.GroupId == id);

            if (GroupTirePrice == null)
            {
                return NotFound();
            }
           ViewData["GroupId"] = new SelectList(_context.Groups, "Id", "Name");
           ViewData["TireId"] = new SelectList(_context.Tires, "Id", "Discriminator");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(GroupTirePrice).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GroupTirePriceExists(GroupTirePrice.GroupId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool GroupTirePriceExists(int id)
        {
            return _context.GroupTirePrices.Any(e => e.GroupId == id);
        }
    }
}
