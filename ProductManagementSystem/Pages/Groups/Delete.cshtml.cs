﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Catalog.Data.Core.Models;
using Catalog.Data.Persistence;

namespace ProductManagementSystem
{
    public class DeleteModel : PageModel
    {
        private readonly Catalog.Data.Persistence.CatalogDatabaseContext _context;

        public DeleteModel(Catalog.Data.Persistence.CatalogDatabaseContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Group Group { get; set; }
        public string ErrorMessage { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return NotFound();
            }

            Group = await _context.Groups
                .Include(group => group.Parent)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.Id == id);

            if (Group == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ErrorMessage = "Delete failed for unknown reason. Try again.";
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Group = await _context.Groups.FindAsync(id);

            if (Group == null)
            {
                return NotFound();
            }

            try
            {
                _context.Groups.Remove(Group);
                await _context.SaveChangesAsync();
                return RedirectToPage("./Index");
            }
            catch (DbUpdateException /* ex */)
            {
                // Log the error (uncomment ex variable name and write a log)
                return RedirectToAction("./Delete", new { id = id, saveChangesError = true });
            }
        }
    }
}
