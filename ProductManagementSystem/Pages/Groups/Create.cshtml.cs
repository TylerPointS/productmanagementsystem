﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Catalog.Data.Core.Models;
using Catalog.Data.Persistence;

namespace ProductManagementSystem
{
    public class CreateModel : PageModel
    {
        private readonly Catalog.Data.Persistence.CatalogDatabaseContext _context;

        public CreateModel(Catalog.Data.Persistence.CatalogDatabaseContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["ParentId"] = new SelectList(_context.Groups, "Id", "Name");
            return Page();
        }

        [BindProperty]
        public Group Group { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var emptyGroup = new Group();

            if (await TryUpdateModelAsync<Group>(
                emptyGroup,
                "group",
                g => g.Name,
                g => g.ParentId,
                g => g.Description))
            {
                Group.DateCreated = DateTime.UtcNow;
                Group.DateCreatedByUserId = 1; //TODO Change hardcoding to actual user id
                Group.DateLastModified = DateTime.UtcNow;
                Group.DateLastModifiedByUserId = 1; //TODO Change hardcoding to actual user id

                _context.Groups.Add(Group);
                await _context.SaveChangesAsync();

                return RedirectToPage("./Index");
            }

            return null;
        }
    }
}