﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Catalog.Data.Core.Models;
using Catalog.Data.Persistence;

namespace ProductManagementSystem
{
    public class IndexModel : PageModel
    {
        private readonly Catalog.Data.Persistence.CatalogDatabaseContext _context;

        public IndexModel(Catalog.Data.Persistence.CatalogDatabaseContext context)
        {
            _context = context;
        }

        public IList<Group> Group { get;set; }
        public string NameSort { get; set; }
        public string DescriptionSort { get; set; }
        public string DateCreatedSort { get; set; }
        public string DateCreatedByUserIdSort { get; set; }
        public string DateLastModifiedSort { get; set; }
        public string DateLastModifiedByUserIdSort { get; set; }
        public string ParentSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }

        public async Task OnGetAsync(string sortOrder)
        {
            NameSort = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            DescriptionSort = sortOrder == "Description" ? "description_desc" : "Description";

            IQueryable<Group> groupQ = _context.Groups.AsQueryable().Select(x => x);

            switch(sortOrder)
            {
                case "name_desc":
                    groupQ = groupQ.OrderByDescending(g => g.Name);
                    break;
                case "Description":
                    groupQ = groupQ.OrderBy(g => g.Description);
                    break;
                case "description_desc":
                    groupQ = groupQ.OrderByDescending(g => g.Description);
                    break;
                default:
                    groupQ = groupQ.OrderBy(g => g.Name);
                    break;
            }

            Group = await groupQ.AsNoTracking()
                .Include(group => group.Parent).ToListAsync();
        }
    }
}
