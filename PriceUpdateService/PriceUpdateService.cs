﻿using Catalog.Data.Core.Models;
using Catalog.Data.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PriceUpdateService
{
    public class PriceUpdateService
    {
        private readonly CatalogDatabaseContext _context;
        private readonly UnitOfWork _unitOfWork;

        public PriceUpdateService(CatalogDatabaseContext context)
        {
            _context = context;
            _unitOfWork = new UnitOfWork(_context);
        }

        public PriceUpdateService(CatalogDatabaseContext context, DateTime testDate)
        {
            _context = context;
            _unitOfWork = new UnitOfWork(_context);
        }

        public void RunPriceUpdateService()
        {
            var rootGroup = new Group();

            try
            {
                rootGroup = GetRootGroup();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            
            CalculateGroupPrices(rootGroup);
            ProcessGroupChildren(rootGroup);

            _unitOfWork.Complete();
        }

        public Group GetRootGroup()
        {

            var rootGroup = _unitOfWork.Groups.SingleOrDefault(g => g.ParentId == 0);

            if (rootGroup == null)
            {
                throw new NullReferenceException("No root group found.");
            }

            return rootGroup;
        }

        public void CalculateGroupPrices(Group group)
        {
            //Clear current group prices
            _unitOfWork.GroupProductPrices.RemoveRange(_unitOfWork.GroupProductPrices.Find(p => p.GroupId == group.Id).ToList());
            _unitOfWork.Complete();

            if (group.ParentId != 0)
            {
                var parentPrices = _unitOfWork.GroupProductPrices.Find(p => p.GroupId == group.ParentId).ToList();

                foreach (var parent in parentPrices)
                {
                    var price = new GroupProductPrice
                    {
                        StartDate = parent.StartDate,
                        EndDate = parent.EndDate,
                        Type = parent.Type,
                        GroupId = group.Id,
                        Group = group,
                        ProductId = parent.ProductId,
                        IsHidden = parent.IsHidden,  //NOTE: We are propogating the hidden state, consider not
                        Price = parent.Price
                    };
                    _unitOfWork.GroupProductPrices.Add(price);
                    //NOTE: Ugly, try to find a more efficient update process
                    _unitOfWork.Complete();
                }
            }

            //Loop through each group price filter order by order
            foreach (var groupPriceFilter in _unitOfWork.GroupPriceFilters.Find(g => group.Id == g.GroupId && g.PriceFilter.IsGlobal == false).OrderBy(o => o.PriceFilter.Order))
            {
                //Is price filter active and valid start date
                if (groupPriceFilter.PriceFilter.IsActive && (Convert.ToDateTime(groupPriceFilter.PriceFilter.StartDate).Date <= DateTime.Now.Date))
                {
                    UpdateGroupProductPrices(groupPriceFilter);
                }
            }

            //Loop through global price filters for group order by order
            foreach (var groupPriceFilter in _unitOfWork.GroupPriceFilters.Find(g => group.Id == g.GroupId && g.PriceFilter.IsGlobal == true).OrderBy(o => o.PriceFilter.Order))
            {
                //Is price filter active and valid start date
                if (groupPriceFilter.PriceFilter.IsActive && (Convert.ToDateTime(groupPriceFilter.PriceFilter.StartDate).Date <= DateTime.Now.Date))
                {
                    UpdateGroupProductPrices(groupPriceFilter);
                }
            }

        }

        private void UpdateGroupProductPrices(GroupPriceFilter groupPriceFilter)
        {
            var priceFilter = _unitOfWork.PriceFilters.GetPriceFilterWithSelectionCriteria(groupPriceFilter.PriceFilterId);

            //Get all items for selection criteria in the price filter including min and max cost
            var products = GetTiresForPriceFilter(priceFilter);

            //Loop through all items
            foreach (var product in products)
            {
                //Execute price change on each item
                ApplyPriceChange(product, groupPriceFilter);
            }
        }

        public ICollection<Tire> GetTiresForPriceFilter(PriceFilter priceFilter)
        {
            var tiresMatchingSelectionCriteria = new List<Tire>();
            string criteriaValue = "";

            foreach (var criteria in priceFilter.SelectionCriteria)
            {
                //Add products where it matches Category, Brand, Model, Item Number
                //Add products where it matches the User tag
                switch (criteria.Source)
                {
                    case "Products":
                        if (criteria.Name == "IdentificationNumber")
                        {
                            //Get the product for the search value and add it to the return list
                            tiresMatchingSelectionCriteria = tiresMatchingSelectionCriteria
                                .Union(_unitOfWork.Tires
                                    .Find(t => t.IdentificationNumber == criteria.Value))
                                .ToList();
                        }
                        else if (criteria.Name == "PartNumber")
                        {
                            tiresMatchingSelectionCriteria = tiresMatchingSelectionCriteria
                                .Union(_unitOfWork.Tires
                                    .Find(t => t.PartNumber == criteria.Value))
                                .ToList();
                        }
                        else if (criteria.Name == "Model")
                        {
                            tiresMatchingSelectionCriteria = tiresMatchingSelectionCriteria
                                .Union(_unitOfWork.Tires
                                    .Find(t => t.Model == criteria.Value))
                                .ToList();
                        }
                        else if (criteria.Name == "Brand")
                        {
                            //Get the search value
                            criteriaValue = criteria.Value;

                            //Get the product for the search value
                            var tire = _unitOfWork.Tires.Find(t => t.Brand == criteriaValue);

                            tiresMatchingSelectionCriteria = tiresMatchingSelectionCriteria.Union(tire).ToList();
                        }
                        else if (criteria.Name == "VehicleClass")
                        {
                            //Get the search value
                            criteriaValue = criteria.Value;

                            //Get the product for the search value
                            var tire = _unitOfWork.Tires.Find(p => p.VehicleClass == criteriaValue);

                            tiresMatchingSelectionCriteria = tiresMatchingSelectionCriteria.Union(tire).ToList();
                        }
                        break;
                    case "UserProductTags":
                        //Find all products where tag matches user's tagged products
                        var products = _unitOfWork.UserProductTags.FindTires(priceFilter.UserId, criteria.Value);

                        if (products != null && products.Count > 0)
                        {
                            tiresMatchingSelectionCriteria = products.Union(tiresMatchingSelectionCriteria).ToList();
                        }

                        break;
                    default:
                        throw new Exception("Unknown selection criteria.");
                }

            }

            return tiresMatchingSelectionCriteria;
        }

        public void ProcessGroupChildren(Group parent)
        {
            foreach (var group in _unitOfWork.Groups.Find(g => g.ParentId == parent.Id))
            {
                CalculateGroupPrices(group);
                ProcessGroupChildren(group);
            }
        }

        public void ApplyPriceChange(Product product, GroupPriceFilter groupFilter)
        {
            var parentProduct = _unitOfWork.GroupProductPrices.Find(p => p.GroupId == groupFilter.Group.ParentId && p.ProductId == product.Id && p.Type == (PriceTypes)groupFilter.PriceFilter.PriceType).SingleOrDefault();
            var currentProduct = _unitOfWork.GroupProductPrices.Find(p => p.GroupId == groupFilter.GroupId && p.ProductId == product.Id && p.Type == (PriceTypes)groupFilter.PriceFilter.PriceType).SingleOrDefault();
            var minimumCost = (decimal)groupFilter.PriceFilter.MinimumCost;
            var maximumCost = (decimal)groupFilter.PriceFilter.MaximumCost;
            var fixedPrice = groupFilter.PriceFilter.FixedPrice;

            var total = 0m;
            var basis = 0m;

            if (currentProduct != null || parentProduct != null)
            {
                total = currentProduct.Price;
            }

            if (parentProduct != null)
            {
                basis = parentProduct.Price;
            }

            if (maximumCost <= 0 && basis > maximumCost && basis < minimumCost)
            {
                return;
            }

            //If the filter is compound, look for existing group product price
            //Fallback to parent if no price found
            if (!groupFilter.PriceFilter.IsCompound)
            {
                total = basis;
            }

            //If fixed price, set fixed price
            if (groupFilter.PriceFilter.FixedPrice > 0)
            {
                total = (decimal)fixedPrice;
            }
            else if (groupFilter.PriceFilter.MarginRate > 0) // This may be fragile since I am not checking for exclusivity
            {
                total = CalculateMargin(total, (decimal)groupFilter.PriceFilter.MarginRate);
            }
            else if (groupFilter.PriceFilter.MarkupRate > 0)
            {
                total = CalculateMarkup(total, (decimal)groupFilter.PriceFilter.MarkupRate);
            }
            else
            {
            }

            //Add additional profit
            if (groupFilter.PriceFilter.AdditionalProfit > 0)
            {
                total += (decimal)groupFilter.PriceFilter.AdditionalProfit;
            }

            var profit = total - basis;
            //check minimum profit
            if (groupFilter.PriceFilter.MinimumProfit > 0 && profit < groupFilter.PriceFilter.MinimumProfit)
            {
                total = (decimal)groupFilter.PriceFilter.MinimumProfit + basis;
            }

            //If there is a fixed cent value, set fixed cent
            if (groupFilter.PriceFilter.FixedCentValue > 0)
            {
                total = SetFixedCentValue(total, (decimal)groupFilter.PriceFilter.FixedCentValue);
            }

            var price = _unitOfWork.GroupProductPrices.Get(groupFilter.GroupId, product.Id, (PriceTypes)groupFilter.PriceFilter.PriceType);

            if (price != null)
            {
                price.Price = total;
            }
            else
            {
                price = new GroupProductPrice
                {
                    StartDate = (DateTime)groupFilter.PriceFilter.StartDate,
                    EndDate = groupFilter.PriceFilter.EndDate,
                    Type = (PriceTypes)groupFilter.PriceFilter.PriceType,
                    GroupId = groupFilter.GroupId,
                    Group = groupFilter.Group,
                    ProductId = product.Id,
                    IsHidden = groupFilter.PriceFilter.IsHidden,
                    Price = total
                };
                _unitOfWork.GroupProductPrices.Add(price);
            }

            _unitOfWork.Complete();
        }

        public static decimal SetFixedCentValue(decimal value, decimal centValue)
        {
            var result = Math.Truncate(value);

            result += centValue;

            return result;
        }

        public static decimal CalculateMargin(decimal cost, decimal marginRate)
        {
            return Math.Round(cost / (1 - marginRate), 2);
        }

        public static decimal CalculateMarkup(decimal cost, decimal markupRate)
        {
            return Math.Round(cost + (cost * markupRate), 2);
        }
    }
}
