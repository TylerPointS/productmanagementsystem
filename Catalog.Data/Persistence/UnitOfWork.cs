﻿using Catalog.Data.Core;
using Catalog.Data.Core.Repositories;
using Catalog.Data.Persistence.Repositories;

namespace Catalog.Data.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CatalogDatabaseContext _context;
        public IGroupPriceFilterRepository GroupPriceFilters { get; set; }
        public IGroupProductPriceRepository GroupProductPrices { get; set; }
        public IGroupTirePriceRepository GroupTirePrices { get; set; }
        public IGroupRepository Groups { get; private set; }
        public IImageRepository Images { get; private set; }
        public IPriceFilterRepository PriceFilters { get; private set; }
        public IProductRepository Products { get; private set; }
        public ISelectionCriteriaRepository SelectionCriteria { get; private set; }
        public ITireRepository Tires { get; set; }
        public IUserProductTagRepository Tags { get; private set; }
        public IUserProductTagRepository UserProductTags { get; set; }

        public UnitOfWork(CatalogDatabaseContext context)
        {
            _context = context;
            GroupPriceFilters = new GroupPriceFilterRepository(_context);
            GroupProductPrices = new GroupProductPriceRepository(_context);
            GroupTirePrices = new GroupTirePriceRepository(_context);
            Groups = new GroupRepository(_context);
            Images = new ImageRepository(_context);
            PriceFilters = new PriceFilterRepository(_context);
            Products = new ProductRepository(_context);
            SelectionCriteria = new SelectionCriteriaRepository(_context);
            Tags = new UserProductTagRepository(_context);
            Tires = new TireRepository(_context);
            UserProductTags = new UserProductTagRepository(_context);
        }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
