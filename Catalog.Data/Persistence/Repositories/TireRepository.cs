﻿using Catalog.Data.Core.Models;
using Catalog.Data.Core.Repositories;

namespace Catalog.Data.Persistence.Repositories
{
    public class TireRepository : Repository<Tire>, ITireRepository
    {
        public CatalogDatabaseContext CatalogContext
        {
            get { return _context as CatalogDatabaseContext; }
        }

        public TireRepository(CatalogDatabaseContext context) : base(context)
        {
        }
    }
}
