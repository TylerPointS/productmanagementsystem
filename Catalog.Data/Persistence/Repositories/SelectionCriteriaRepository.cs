﻿using Catalog.Data.Core.Models;
using Catalog.Data.Core.Repositories;

namespace Catalog.Data.Persistence.Repositories
{
    public class SelectionCriteriaRepository : Repository<SelectionCriteria>, ISelectionCriteriaRepository
    {
        public CatalogDatabaseContext CatalogContext
        {
            get { return _context as CatalogDatabaseContext; }
        }

        public SelectionCriteriaRepository(CatalogDatabaseContext context) : base(context)
        {
        }
    }
}
