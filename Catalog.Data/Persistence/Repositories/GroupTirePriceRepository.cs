﻿using Catalog.Data.Core.Models;
using Catalog.Data.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Catalog.Data.Persistence.Repositories
{
    class GroupTirePriceRepository : Repository<GroupTirePrice>, IGroupTirePriceRepository
    {
        public CatalogDatabaseContext CatalogContext
        {
            get { return _context as CatalogDatabaseContext; }
        }

        public GroupTirePriceRepository(CatalogDatabaseContext context) : base(context)
        {
        }

        public GroupTirePrice Get(int groupId, int tireId, int price)
        {
            return CatalogContext.GroupTirePrices.Find(groupId, tireId, price);
        }
    }
}