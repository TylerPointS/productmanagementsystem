﻿using Catalog.Data.Core.Models;
using Catalog.Data.Core.Repositories;
using System;
using System.Collections.Generic;

namespace Catalog.Data.Persistence.Repositories
{
    public class GroupProductPriceRepository : Repository<GroupProductPrice>, IGroupProductPriceRepository
    {
        public CatalogDatabaseContext CatalogContext
        {
            get { return _context as CatalogDatabaseContext; }
        }

        public GroupProductPriceRepository(CatalogDatabaseContext context) : base(context)
        {
        }

        public GroupProductPrice Get(int groupId, int productId, PriceTypes type)
        {
            return CatalogContext.GroupProductPrices.Find(groupId, productId, type);
        }

        public IEnumerable<GroupProductPrice> GetAllWIthProducts ()
        {
            var groupProductPrice = GetAll();

            foreach (var item in groupProductPrice)
            {
                CatalogContext.Entry(item)
                    .Reference(i => i.Product)
                    .Load();
            }

            return groupProductPrice;
        }
    }
}
