﻿using Catalog.Data.Core.Models;
using Catalog.Data.Core.Repositories;

namespace Catalog.Data.Persistence.Repositories
{
    public class ImageRepository : Repository<Image>, IImageRepository
    {
        public CatalogDatabaseContext CatalogContext
        {
            get { return _context as CatalogDatabaseContext; }
        }

        public ImageRepository(CatalogDatabaseContext context) : base(context)
        {
        }
    }
}
