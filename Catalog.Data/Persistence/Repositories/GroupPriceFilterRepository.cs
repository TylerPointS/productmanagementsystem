﻿using Catalog.Data.Core.Models;
using Catalog.Data.Core.Repositories;

namespace Catalog.Data.Persistence.Repositories
{
    public class GroupPriceFilterRepository : Repository<GroupPriceFilter>, IGroupPriceFilterRepository
    {
        public CatalogDatabaseContext CatalogContext
        {
            get { return _context as CatalogDatabaseContext; }
        }

        public GroupPriceFilterRepository(CatalogDatabaseContext context) : base(context)
        {
        }

        public GroupPriceFilter Get(int groupId, int priceFilterId)
        {
            return CatalogContext.GroupPriceFilters.Find(groupId, priceFilterId);
        }
    }
}
