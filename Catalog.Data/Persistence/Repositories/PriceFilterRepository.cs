﻿using Catalog.Data.Core.Models;
using Catalog.Data.Core.Repositories;

namespace Catalog.Data.Persistence.Repositories
{
    public class PriceFilterRepository : Repository<PriceFilter>, IPriceFilterRepository
    {
        public CatalogDatabaseContext CatalogContext
        {
            get { return _context as CatalogDatabaseContext; }
        }

        public PriceFilterRepository(CatalogDatabaseContext context) : base(context)
        {
        }

        public PriceFilter GetPriceFilterWithSelectionCriteria(int priceFilterId)
        {
            var priceFilter = _context.PriceFilters.Find(priceFilterId);

            _context.Entry(priceFilter)
                .Collection(p => p.SelectionCriteria)
                .Load();

            return priceFilter;
        }
    }
}
