﻿using Catalog.Data.Core.Models;
using Catalog.Data.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Catalog.Data.Persistence.Repositories
{
    class UserProductTagRepository : Repository<UserProductTag>, IUserProductTagRepository
    {
        public CatalogDatabaseContext CatalogContext
        {
            get { return _context as CatalogDatabaseContext; }
        }

        public UserProductTagRepository(CatalogDatabaseContext context) : base(context)
        {
        }

        public ICollection<Tire> FindTires(int userId, string tag)
        {
            var userProductTags = _context.UserProductTags
                .Include(p => p.Product)
                .Where(t => t.Tag == tag && t.UserId == userId);

            var tires = new List<Tire>();

            foreach (var productTag in userProductTags)
            {
                tires.Add((Tire)productTag.Product);
            }

            return tires;
        }
    }
}
