﻿using Catalog.Data.Core.Models;
using Catalog.Data.Core.Repositories;

namespace Catalog.Data.Persistence.Repositories
{
    public class GroupRepository : Repository<Group>, IGroupRepository
    {
        public CatalogDatabaseContext CatalogContext
        {
            get { return _context as CatalogDatabaseContext; }
        }

        public GroupRepository(CatalogDatabaseContext context) : base(context)
        {
        }

        public Group GetWithPriceFIlters(int groupId)
        {
            var group = Get(groupId);

            CatalogContext.Entry(group)
                .Collection(g => g.PriceFilters)
                .Load();

            return group;
        }
    }
}
