﻿using Catalog.Data.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Catalog.Data.Persistence
{
    public static class DatabaseInitializer
    {
        public static void Initialize(CatalogDatabaseContext context)
        {
            context.Database.EnsureCreated();
            
            if (context.Tires.Any())
            {
                return; //Database has been seeded.
            }
        }
    }
}
