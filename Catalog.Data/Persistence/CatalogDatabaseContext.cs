﻿using Catalog.Data.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Catalog.Data.Persistence
{
    public class CatalogDatabaseContext : DbContext
    {
        public CatalogDatabaseContext(DbContextOptions<CatalogDatabaseContext> options) : base(options)
        {

        }

        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<GroupPriceFilter> GroupPriceFilters { get; set; }
        public virtual DbSet<GroupProductPrice> GroupProductPrices { get; set; }
        public virtual DbSet<GroupTirePrice> GroupTirePrices { get; set; }
        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<PriceFilter> PriceFilters { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<SelectionCriteria> SelectionCriteria { get; set; }
        public virtual DbSet<UserProductTag> UserProductTags { get; set; }
        public virtual DbSet<Tire> Tires { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GroupPriceFilter>()
                .HasKey(gr => new { gr.GroupId, gr.PriceFilterId });
            modelBuilder.Entity<GroupProductPrice>()
                .HasKey(gr => new { gr.GroupId, gr.ProductId, gr.Type });
            modelBuilder.Entity<GroupTirePrice>()
                .HasKey(gr => new { gr.GroupId, gr.TireId, gr.Type });
        }
    }
}
