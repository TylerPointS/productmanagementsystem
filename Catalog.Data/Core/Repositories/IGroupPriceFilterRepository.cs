﻿using Catalog.Data.Core.Models;

namespace Catalog.Data.Core.Repositories
{
    public interface IGroupPriceFilterRepository : IRepository<GroupPriceFilter>
    {
        GroupPriceFilter Get(int groupId, int priceFilterId);
    }
}
