﻿using Catalog.Data.Core.Models;

namespace Catalog.Data.Core.Repositories
{
    public interface IImageRepository : IRepository<Image>
    {
    }
}
