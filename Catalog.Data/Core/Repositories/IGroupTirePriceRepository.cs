﻿using Catalog.Data.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Catalog.Data.Core.Repositories
{
    public interface IGroupTirePriceRepository : IRepository<GroupTirePrice>
    {
        GroupTirePrice Get(int groupId, int tireId, int type);
    }
}
