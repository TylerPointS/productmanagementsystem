﻿using Catalog.Data.Core.Models;

namespace Catalog.Data.Core.Repositories
{
    public interface ISelectionCriteriaRepository : IRepository<SelectionCriteria>
    {
    }
}
