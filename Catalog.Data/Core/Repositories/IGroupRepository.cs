﻿using Catalog.Data.Core.Models;

namespace Catalog.Data.Core.Repositories
{
    public interface IGroupRepository : IRepository<Group>
    {
        Group GetWithPriceFIlters(int groupId);
    }
}
