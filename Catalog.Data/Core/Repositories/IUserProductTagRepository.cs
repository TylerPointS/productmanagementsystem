﻿using Catalog.Data.Core.Models;
using System.Collections.Generic;

namespace Catalog.Data.Core.Repositories
{
    public interface IUserProductTagRepository : IRepository<UserProductTag>
    {
        ICollection<Tire> FindTires(int userId, string tag);
    }
}
