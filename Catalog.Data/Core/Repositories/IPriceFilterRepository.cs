﻿using Catalog.Data.Core.Models;

namespace Catalog.Data.Core.Repositories
{
    public interface IPriceFilterRepository : IRepository<PriceFilter>
    {
        PriceFilter GetPriceFilterWithSelectionCriteria(int id);
    }
}
