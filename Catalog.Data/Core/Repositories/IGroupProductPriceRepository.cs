﻿using Catalog.Data.Core.Models;
using System.Collections.Generic;

namespace Catalog.Data.Core.Repositories
{
    public interface IGroupProductPriceRepository : IRepository<GroupProductPrice>
    {
        GroupProductPrice Get(int groupId, int productId, PriceTypes type);
        IEnumerable<GroupProductPrice> GetAllWIthProducts();
    }
}
