﻿using Catalog.Data.Core.Repositories;
using System;

namespace Catalog.Data.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IGroupPriceFilterRepository GroupPriceFilters { get; }
        IGroupProductPriceRepository GroupProductPrices { get; }
        IGroupTirePriceRepository GroupTirePrices { get; }
        IGroupRepository Groups { get; }
        IImageRepository Images { get; }
        IPriceFilterRepository PriceFilters { get; }
        IProductRepository Products { get; }
        ISelectionCriteriaRepository SelectionCriteria { get; }
        ITireRepository Tires { get; }
        IUserProductTagRepository Tags { get; }
        IUserProductTagRepository UserProductTags { get; }
        int Complete();
    }
}
