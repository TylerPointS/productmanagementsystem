﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Catalog.Data.Core.Models
{
    public class PriceFilter
    {
        // Metadata
        public int Id { get; set; }
        public string Name { get; set; }
        [DisplayName("Active")]
        public bool IsActive { get; set; }
        [DisplayName("Global")]
        public bool IsGlobal { get; set; }
        [DisplayName("Hidden")]
        public bool IsHidden { get; set; }
        [DisplayName("Compound")]
        public bool IsCompound { get; set; }
        public int? Order { get; set; }
        public int UserId { get; set; }

        // Selection criteria
        public decimal MinimumCost { get; set; }
        public decimal MaximumCost { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public PriceTypes PriceType { get; set; }

        // Price Change
        public decimal FixedPrice { get; set; }
        //Total = Cost / (1 - Margin)
        public decimal MarginRate { get; set; }
        //Total = (Cost * Markup) + Cost
        public decimal MarkupRate { get; set; }
        public decimal AdditionalProfit { get; set; }
        public decimal MinimumProfit { get; set; }
        public decimal? FixedCentValue { get; set; }

        // Audit fields
        public DateTime DateCreated { get; set; }
        public int DateCreatedByUserId { get; set; }
        public DateTime DateLastModified { get; set; }
        public int DateLastModifiedByUserId { get; set; }

        public virtual ICollection<GroupPriceFilter> Groups { get; set; }
        public virtual ICollection<SelectionCriteria> SelectionCriteria { get; set; }
    }
}
