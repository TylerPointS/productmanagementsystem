﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Catalog.Data.Core.Models
{
    public class Group
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public int? ParentId { get; set; }

        public DateTime DateCreated { get; set; }
        public int DateCreatedByUserId { get; set; }
        public DateTime DateLastModified { get; set; }
        public int DateLastModifiedByUserId { get; set; }

        [ForeignKey("ParentId")]
        public virtual Group Parent { get; set; }
        public virtual ICollection<GroupPriceFilter> PriceFilters { get; set; }
        public virtual ICollection<GroupProductPrice> ProductPrices { get; set; }

        public string LocalDateCreated
        {
            get
            {
                return DateCreated.ToLocalTime().ToShortDateString();
            }
        }

        public string LocalDateLastModified
        {
            get
            {
                return DateLastModified.ToLocalTime().ToShortDateString();
            }
        }
    }
}
