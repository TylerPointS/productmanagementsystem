﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Catalog.Data.Core.Models
{
    public class GroupTirePrice
    {
        public decimal Price { get; set; }
        public DateTime StartDate { get; set; }
        [Key]
        [Column(Order = 2)]
        public int Type { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsHidden { get; set; }

        [Key]
        [Column(Order = 0)]
        [ForeignKey("Group")]
        public int GroupId { get; set; }
        [Key]
        [Column(Order = 1)]
        [ForeignKey("Tire")]
        public int TireId { get; set; }

        public virtual Group Group { get; set; }
        public virtual Tire Tire { get; set; }
    }

    [Flags]
    public enum PriceTypes
    {
        Default = 0,
        List = 1,
        Discount = 2,
        Special = 3,
        Overbill = 4,
        Other = 5
    }
}
