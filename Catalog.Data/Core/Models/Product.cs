﻿using System;
using System.Collections.Generic;

namespace Catalog.Data.Core.Models
{
    public abstract class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Features { get; set; }
        public string Benefits { get; set; }
        public string Brand { get; set; }   //e.g. Bridgestone
        public string Series { get; set; }  //e.g. Potenza
        public string Model { get; set; }   //e.g. RE040
        public string IdentificationNumber { get; set; }
        public decimal? QuantityPerUnit { get; set; }
        public decimal? Length { get; set; }
        public decimal? Width { get; set; }
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }

        public DateTime DateCreated { get; set; }
        public int DateCreatedByUserId { get; set; }
        public DateTime DateLastModified { get; set; }
        public int DateLastModifiedByUserId { get; set; }

        public virtual ICollection<GroupProductPrice> Groups { get; set; }
        public virtual ICollection<Image> Images { get; set; }  //URLs for images for each tire
    }
}
