﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Catalog.Data.Core.Models
{
    public class Image
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public byte? Contents { get; set; }

        public DateTime DateCreated { get; set; }
        public int DateCreatedByUserId { get; set; }
        public DateTime DateLastModified { get; set; }
        public int DateLastModifiedByUserId { get; set; }

        [ForeignKey("Product")]
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}
