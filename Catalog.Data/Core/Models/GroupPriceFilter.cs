﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Catalog.Data.Core.Models
{
    public class GroupPriceFilter
    {
        [Key]
        [Column(Order = 0)]
        [ForeignKey("Group")]
        public int GroupId { get; set; }

        public virtual Group Group { get; set; }

        [Key]
        [Column(Order = 1)]
        [ForeignKey("PriceFilter")]
        public int PriceFilterId { get; set; }

        public virtual PriceFilter PriceFilter { get; set; }
    }
}
