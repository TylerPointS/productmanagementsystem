﻿using System;
using System.ComponentModel;
using System.Linq;

namespace Catalog.Data.Core.Models
{
    public class Tire : Product
    {
        [DisplayName("Vehicle Class")]
        public string VehicleClass { get; set; }
        public string Prefix { get; set; }
        public string SectionWidth { get; set; }
        public string SidewallAspectRatio { get; set; }
        public string WheelDiameter { get; set; }
        public string InternalConstruction { get; set; }
        public int? LoadIndex { get; set; }
        public int MaximumTirePressure { get; set; }
        public string SpeedRating { get; set; }
        public string PlyRating { get; set; }
        public string Sidewall { get; set; }
        public string TreadDepth { get; set; }
        public string UniformTireQualityGrading { get; set; }
        public int? Mileage { get; set; }
        public string PartNumber { get; set; }
        public string ManufacturerPartNumber { get; set; }
        public decimal FederalExciseTax { get; set; }

        public string VehicleClassDisplay
        {
            get
            {
                if (VehicleClass == null)
                {
                    return "Unknown";
                }

                switch (VehicleClass.ToUpper())
                {
                    case "PASS":
                        return "Passenger";
                    case "LT":
                        return "Light Truck";
                    case "OTR":
                        return "Off The Road";
                    case "INDUST":
                        return "Industrial";
                    case "FARM":
                        return "Farm";
                    case "ATV":
                        return "All Terrain Vehicle";
                    case "MTRUCK":
                        return "Commercial";
                    case "TRAILER":
                        return "Trailer";
                    case "WINTER":
                        return "Winter";
                    default:
                        break;
                }

                return "Unknown";
            }
        }

        public string Size
        {
            get
            {
                return SectionWidth + SidewallAspectRatio + WheelDiameter;
            }
        }

        [DisplayName("Size")]
        public string DisplaySize
        {
            get
            {
                var separator = "";

                if (VehicleClass == "PASS" || VehicleClass == "LT")
                {
                    separator = "/";
                }
                return SectionWidth + separator + SidewallAspectRatio + InternalConstruction + WheelDiameter;
            }
        }
    }
}
