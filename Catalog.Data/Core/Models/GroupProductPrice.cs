﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Catalog.Data.Core.Models
{
    public class GroupProductPrice
    {
        public decimal Price { get; set; }
        public DateTime StartDate { get; set; }
        [Key]
        [Column(Order = 2)]
        public PriceTypes Type { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsHidden { get; set; }

        [Key]
        [Column(Order = 0)]
        [ForeignKey("Group")]
        public int GroupId { get; set; }
        [Key]
        [Column(Order = 1)]
        [ForeignKey("Product")]
        public int ProductId { get; set; }

        public virtual Group Group { get; set; }
        public virtual Product Product { get; set; }
    }
}
