﻿namespace Catalog.Data.Core.Models
{
    public class UserProductTag
    {
        public int Id { get; set; }
        public string Tag { get; set; }
        public int Order { get; set; }
        public int UserId { get; set; }
        public int ProductId { get; set; }

        public virtual Product Product { get; set; }
    }
}
