﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Catalog.Data.Core.Models
{
    public class SelectionCriteria
    {
        public int Id { get; set; }
        public string Source { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }


        [ForeignKey("Product")]
        public int PriceFilterId { get; set; }
        public virtual PriceFilter PriceFilter { get; set; }
    }
}
