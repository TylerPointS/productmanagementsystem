﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Catalog.Data.Core.Models
{
    public class PriceImport
    {
        public string ManufacturerPartNumber { get; set; }
        [Required]
        public decimal Price { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? IsValid { get; set; }
        public DateTime? DateProcessed { get; set; }
        [Key]
        public DateTime DateCreated { get; set; }
        public int GroupId { get; set; }
        public int UserId { get; set; }
    }
}
